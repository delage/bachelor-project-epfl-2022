# MicroSIG
Synthetic image generator (SIG) for defocused/astigmatic particle images.

MicroSIG provides realistic particle images for testing PIV/PTV methods involving defocusing or astigmatic particle images. This includes for instance micro-PIV experiments or 3D PTV methods using defocusing or astigmatism to retrieve the out-of-plane particle position. MicroSIG is based on an approximated model of spherical lens and simple ray tracing.

More detail about the MicroSIG can be found in "M. Rossi, *Synthetic image generator for defocusing and astigmatic PIV/PTV*, Meas. Sci. Technol., **31**, 017003 (2020) [DOI:10.1088/1361-6501/ab42bb](https://doi.org/10.1088/1361-6501/ab42bb)".


### How it works:
1. Run MicroSIG: From your local directory, run the file `microsig.m` (or type `microsig` in the command line if the directory was added to your preference path).
2. Set the `using_comsol` variable to `0` or `1` depending on what you are using.
    * If you use Comsol, Follow the instructions and select: the setting file (.txt), the X, Y and Z (.csv) files, and the destination folder for the images.
    * If you do not use COMSOL, Follow the instructions and select: the setting file (.txt), the data files (.txt), and the destination folder for the images.
3. The images in `.tif` format will be created in the *destination folder*.

### Setting files:
Setting files must be csv file (`.txt`) in `ASCII` format, each row must be a comma-separated line with: `parameter, value`.
MicroSIG uses 17 parameters:
* `magnification`: Magnification of the simulated lens.
* `numerical_aperture`: Numerical aperture of the simulated lens.
* `focal_length`: This parameter must be chosen empirically from comparison with real images.
* `ri_medium`: Refractive index of the immersion medium of the lens.
* `ri_lens`: Refractive index of the immersion medium of the lens.
* `pixel_size`: Size of the side of a square pixel (in microns).
* `pixel_dim_x`: Number of pixels in the x-direction of the sensor.
* `pixel_dim_y`: Number of pixels in the y-direction of the sensor.
* `background_mean`: Constant value of the image background.
* `background_noise`: Amplitude of Gaussian noise added to the images.
* `points_per_pixel`: Number of point sources in a particle, normalized for the particle area. Decrease this parameter for large particles. Typical values are between 10 and 20. 
* `n_rays`: Number of rays for point source. Decrease this value to speed up the particle computation. Typical values are between 100 and 500.
* `gain`: Additional gain to increase or decrease the image intensity.
* `cyl_focal_length`: Additional parameter for astigmatic imaging. This parameter must be chosen empirically from comparison with real images. A typical value is 4000. When it is set to 0 no astigmatism is present. 
* `z_min`: Minimum depth coordinate (in µm). It places the 0 for the Z coordinate with respect to the focal plane. Positive values are towards the lens.
* `z_depth`: Total depth of the measurement volume (in µm).
* `dp`: Diameter of tracer particles (in µm). 

### Data files:
Data files must be csv files (`.txt` or `.csv`) in `ASCII` format. A header containing the variable names must be included and the values in each row must be comma-separated.  

If you do not use COMSOL, each column represents a variable, each row represents one particle. Four variables are required: 
* `fr`: number of frame in which the particle is contained.
* `X`: horizontal particle coordinate (in pixels).
* `Y`: vertical particle coordinate (in pixels).
* `Z`: out-of-plane coordinate in normalized units with 0 correpsonding to `z_min` and 1 corresponding to `z_min`+`z_depth`.

Optional variables:
* `dp`: particle diameter (in µm). It can be used to have non-monodispersed particles. If this value is omitted, the `dp` in the setting file is used.
* `c_int`: multiplication factor for the particle image intensity (simulating not-uniform illumination). 
* `ep`: elongation factor for spheroidal particles (vertical axis/horizontal axis)
* `alpha`, `beta`: orientation angles for non-spherical particles (these values are required if `ep` is included).  

If you use COMSOL you should have 3 `.csv` files you got from exporting the particles position tables.  
Each file corresponds to one coordinate, and starts with a header, followed by the matrix of data, with each row corresponding to a time frame and each column to a particle.  
It is not possible to use the optional variables with this manner, however, a file called `real_pos_table.txt` will be generated for you. It can be used to run MicroSIG with the other method, enabling you to use optional variables.
