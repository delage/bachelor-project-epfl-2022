function calib_stack_creator(varargin)
% calib_stack_creator generates a calibration stack based on the first
% frame from the data provided, and the channel height provided
% calib_stack_creator only works with comsol files as input (see microsig
% when using_comsol = 1)

%change channel_height according to the height you want the calibration
%stack to cover
channel_height = 40;
n_cal = 100; % number of images you want in the calibration stack

%add all defocustracker functions to Matlab path
addpath(genpath('.//../'));

[filename, pathname] = uigetfile('*.txt','select settings (*.txt)');
if filename==0, return, end
name_mic = [pathname,filename];

    %comsol files are required
    [filename, pathname] = uigetfile('*.csv','Select X data (*.csv)');
    xPosFile = [pathname,filename];
    [filename, pathname] = uigetfile('*.csv','Select Y data (*.csv)');
    yPosFile = [pathname,filename];

    folder_images = uigetdir('','Select destination folder');
    if folder_images==0, return, end

    x_coord = readmatrix(xPosFile);
    y_coord = readmatrix(yPosFile);

    temp = size(x_coord);
    x_coord = x_coord(:, 2 : temp(2));
    y_coord = y_coord(:, 2 : temp(2));

    n_frames = temp(1);
    n_particles = temp(2)-1;

    X = zeros(n_frames*n_particles, 1);
    Y = zeros(n_frames*n_particles, 1);
    Z = zeros(n_frames*n_particles, 1);
    fr = zeros(n_frames*n_particles, 1);

    for i = 1:n_cal
        fr((i-1)*n_particles+1:i*n_particles) = i;
        X((i-1)*n_particles+1:i*n_particles) = transpose(x_coord(1, :));
        Y((i-1)*n_particles+1:i*n_particles) = transpose(y_coord(1, :));
        Z((i-1)*n_particles+1:i*n_particles) = (i-1)*channel_height/(n_cal-1);
    end

    dat = table(fr, X, Y, Z);

if ischar(name_mic)
    mic = readtable(name_mic,'ReadRowNames',true);
elseif istable(name_mic)
    mic = name_mic;
else
    disp('Error: Settings file not compatible.')
end

if height(mic)==17
    mic = table2struct(rows2vars(mic));
else
    disp('Error: Settings file not compatible.')
    return
end

N_tot = height(dat);
P = zeros(N_tot,4);
fr = dat.fr;
P(:,1) = dat.X;
P(:,2) = dat.Y;
P(:, 3 )= dat.Z + mic.z_min;

if ismember('dp',dat.Properties.VariableNames)
    P(:,4) = dat.dp;
else
    P(:,4) = mic.dp;
end
if ismember('ep',dat.Properties.VariableNames)
    P = [P, dat.ep, dat.alpha, dat.beta];
end
if ismember('c_int',dat.Properties.VariableNames)
    P = [P, dat.c_int];
end


frames = unique(fr);
n_images = length(frames);

n_particles = 0;
tic
for ii = 1:n_images
    disp(['creating image ',num2str(ii),' of ',num2str(n_images),' ...'] )
    flag = fr==frames(ii);
    pos = P(flag,:);
    im_test = take_image(mic,pos);
    n_particles = n_particles+size(pos,1);
    if isempty(folder_images)
        assignin('base',['B',num2str(frames(ii),'%05d')],im_test)
    else
        %this generates background in brightfield with black particles, 
        %remove "256 -" to generate in darkfield with white particles
        imwrite(256 - uint16(im_test),[folder_images,'//B',num2str(frames(ii),'%05d'),'.tif'])
    end
end
time_tot = toc;
disp(['total time: ',num2str(time_tot),' sec'])
disp(['particles per images: ',num2str(n_particles/n_images)])
disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])

imgset = dtracker_create('imageset',folder_images);
dtracker_show(imgset);
