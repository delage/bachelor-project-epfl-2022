function microsig(varargin)
% MICROSIG    Function to generate synthetic particle images
%
%   using_comsol = 0 : 
%
%   MICROSIG starts the procedure for generating synthetic images. The
%   first step is to load the setting file (must be a csv file). The second
%   step is to load the coordinate file (must be a csv file, see readme.md
%   and example for more information). The third step is to select
%   the destination folder of the images. Finally the corresponding
%   synthetic images are generated. Images are names as B00001.tif,
%   B00002.fit and so on.
%
%   MICROSIG(my_setting, my_coordinate, my_destination_folder) create the
%   images in my_destination using my_setting as setting
%   and my_coordinate as coordinate. my_setting/my_coordinate can be given
%   in a form of a csv file or of a MATLAB table.
%
%   MICROSIG(my_setting, my_coordinate) same as above but this time the
%   images in the MATLAB workspace.
%
%   using_comsol = 1
%   create the images by givingas input the setting file and 3 coordinate
%   files, x.csv, y.csv and z.csv, in the format of comsol outpur.
using_comsol = 1;

%add all defocustracker functions to Matlab path
addpath(genpath('.//../'));

if using_comsol == 0
if nargin==2
    name_mic = varargin{1};
    name_dat = varargin{2};
    folder_images = [];
elseif nargin==3
    name_mic = varargin{1};
    name_dat = varargin{2};
    folder_images = varargin{3};
else
    [filename, pathname] = uigetfile('*.txt','select settings (*.txt)');
    if filename==0, return, end
    name_mic = [pathname,filename];
    
    [filename, pathname] = uigetfile('*.txt','Select source data (*.txt)');
    if filename==0, return, end
    name_dat = [pathname,filename];
    
    folder_images = uigetdir('','Select destination folder');
    if folder_images==0, return, end
end

if ischar(name_dat)
    dat = readtable(name_dat);
elseif istable(name_dat)
    dat = name_dat;
else
    disp('Error: Data file not compatible.')
end

else 
    [filename, pathname] = uigetfile('*.txt','select settings (*.txt)');
    if filename==0, return, end
    name_mic = [pathname,filename];

    %comsol files are required
    [filename, pathname] = uigetfile('*.csv','Select X data (*.csv)');
    xPosFile = [pathname,filename];
    [filename, pathname] = uigetfile('*.csv','Select Y data (*.csv)');
    yPosFile = [pathname,filename];
    [filename, pathname] = uigetfile('*.csv','Select Z data (*.csv)');
    zPosFile = [pathname,filename];

    folder_images = uigetdir('','Select destination folder');
    if folder_images==0, return, end

    x_coord = readmatrix(xPosFile);
    y_coord = readmatrix(yPosFile);
    z_coord = readmatrix(zPosFile);

    temp = size(x_coord);
    x_coord = x_coord(:, 2 : temp(2));
    y_coord = y_coord(:, 2 : temp(2));
    z_coord = z_coord(:, 2 : temp(2));
    
    n_frames = temp(1);
    n_particles = temp(2)-1;

    X = zeros(n_frames*n_particles, 1);
    Y = zeros(n_frames*n_particles, 1);
    Z = zeros(n_frames*n_particles, 1);
    fr = zeros(n_frames*n_particles, 1);

    for i = 1:n_frames
        fr((i-1)*n_particles+1:i*n_particles) = i;
        X((i-1)*n_particles+1:i*n_particles) = transpose(x_coord(i, :));
        Y((i-1)*n_particles+1:i*n_particles) = transpose(y_coord(i, :));
        Z((i-1)*n_particles+1:i*n_particles) = transpose(z_coord(i, :));
    end    
    dat = table(fr, X, Y, Z);
end

if ischar(name_mic)
    mic = readtable(name_mic,'ReadRowNames',true);
elseif istable(name_mic)
    mic = name_mic;
else
    disp('Error: Settings file not compatible.')
end

if height(mic)==17
    mic = table2struct(rows2vars(mic));
else
    disp('Error: Settings file not compatible.')
    return
end

N_tot = height(dat);
P = zeros(N_tot,4);
fr = dat.fr;
P(:,1) = dat.X;
P(:,2) = dat.Y;
if using_comsol == 0
    P(:,3) = dat.Z*mic.z_depth+mic.z_min;
else
    P(:,3) = dat.Z + mic.z_min;
    
    %need real simulation position for error comparison
    X = P(:,1);
    Y = P(:, 2);
    Z = (P(:, 3)-mic.z_min)/mic.z_depth; 
    real_pos_table = table(fr, X, Y, Z);
    writetable(real_pos_table, 'real_pos_table.txt') 

end

save('real_positions.mat','P');

if ismember('dp',dat.Properties.VariableNames)
    P(:,4) = dat.dp;
else
    P(:,4) = mic.dp;
end
if ismember('ep',dat.Properties.VariableNames)
    P = [P, dat.ep, dat.alpha, dat.beta];
end
if ismember('c_int',dat.Properties.VariableNames)
    P = [P, dat.c_int];
end


frames = unique(fr);
n_images = length(frames);

n_particles = 0;
tic
for ii = 1:n_images
    disp(['creating image ',num2str(ii),' of ',num2str(n_images),' ...'] )
    flag = fr==frames(ii);
    pos = P(flag,:);
    im_test = take_image(mic,pos);
    n_particles = n_particles+size(pos,1);
    if isempty(folder_images)
        assignin('base',['B',num2str(frames(ii),'%05d')],im_test)
    else
        %this generates background in brightfield with black particles, 
        %remove "256 -" to generate in darkfield with white particles
        imwrite(256 - uint16(im_test),[folder_images,'//B',num2str(frames(ii),'%05d'),'.tif'])
    end
end
time_tot = toc;
disp(['total time: ',num2str(time_tot),' sec'])
disp(['particles per images: ',num2str(n_particles/n_images)])
disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])

imgset = dtracker_create('imageset',folder_images);
dtracker_show(imgset);