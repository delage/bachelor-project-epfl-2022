# def_track_lab
`def_track_lab.m` is a convenient arrangement of defocustracker functions, inspired by the workthrough examples one can find at [defocustracking.com](https://defocustracking.com/).  
All used functions are contained in the `defocustracker` repository, which is added to path in `def_track_lab.m`. If it fails, add it manually.

## DefocusTracker
Software package for 3D-PTV using the General Defocusing Particle Tracking method.

*DefocusTracker* is based on 3 data structures (MATLAB struct) and 5 functions:
* `imageset`: Link to a collection of images.
* `model`: Data and settings required to run one evaluation.
* `dataset`: Data obtained after one evaluation.  
* `dtracker_create()`: Create data structures.
* `dtracker_show()`: Open GUIs to inspect data structures.
* `dtracker_train()`: Train a model on specific training data.
* `dtracker_process()`: Process an imageset using a given model.
* `dtracker_postprocess()`: Manipulate datasets.

*DefocusTracker* is a modular software. The general workflow remains the same, but different approaches or methods can easily be implemented and applied.  

## Requirements
DefocusTracker has been tested on Matlab R2018b and R2021a. The following toolboxes are required:
* curve_fitting_toolbox
* image_toolbox
* statistics_toolbox


