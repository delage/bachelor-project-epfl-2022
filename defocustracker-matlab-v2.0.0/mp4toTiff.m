%this small program enables you to convert a .mp4 video file into a
%sequence of 8-bit .tif images

obj = VideoReader('50FPS_10s_100nlmin001.mp4'); %insert path to mp4 video
vid = read(obj);
frames = obj.NumFrames;
for x = 1 : frames
    imwrite(vid(:,:,:,x),strcat('tiff_images\\frame-',num2str(x),'.tif'));
end