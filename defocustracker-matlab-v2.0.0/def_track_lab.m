%% Setup

clc, clear, close all

%add all defocustracker functions to Matlab path
addpath(genpath('.//defocustracker//'));
%% Create the training Dataset

%Load Calibration Images
myfolder = './/calibration//';
img_train = dtracker_create('imageset',myfolder);

selected_frames = 1:img_train.n_frames;

%Create the training dataset
N_cal = length(selected_frames); %number of images in the calibstack
dat_train = dtracker_create('dataset',N_cal);
dat_train.fr(:) = selected_frames; %array of frames in the training data
dat_train.X(:) = zeros(1,N_cal) + 323.5; %X coordinate of the selected particle
dat_train.Y(:) = zeros(1,N_cal) + 545.5; %Y coordinate of the selected particle
dat_train.Z(:) = linspace(0,1,N_cal); %Z coordinate of the selected particle, should not be changed

%note : dat_train.X and dat_train.Y are vectors of the x and y coordinate 
%of the calibration particle on each frame if your particle happens to move
%during the calibration, you have to keep the calibration point
%centered on it during the whole calibration.

%Inspect the training set, change parameters in this section and run it 
%again if the calibration point is not centered on a particle
dtracker_show(img_train,dat_train)

%% Create the model
%Create the model with default parameters
model = dtracker_create('model');

%Edit the relevant field in training parameters
model.training.imwidth = 80;            % width of the particle image
model.training.imheight = 80;           % height of the particle image
model.training.gauss_filter = 0;        % gauss filter to reduce noise effects
model.training.median_filter = 3;       % median filter to reduce thermal noise
model.training.smoothing = 0.2;         % smoothing (optional)
model.training.n_interp_images = 100;   % enter in this field the numbers of images you want after interpolation
%images will be generated by interpolation to match your number.

%Train the model with the Dataset
model = dtracker_train(model,img_train,dat_train);

%Inspect the model, change parameters in this section and run it again if
%you are not satisfied, especially if the particle does not show up
%entirely in the window, or if several particles show up in the window.

%the part of the image circled out in green is what defocustracker
%interprests as being the particle

%you will also see the signal to noise ratio in top left corner, the
%similarity profile in the top right, and the similarity profile map in the
%bottom right. For a detailed descritption of these parameters, see section
%2 of "a fast and robust algorithm for general defocusing particle 
%tracking" - Massimiliano Rossi & Rune Barnkob, 2020, available at https://arxiv.org/abs/1912.01912
dtracker_show(model)
%% THIS SECTION IS ONLY USEFULL IF ALL THE CALIBRATION PARTICLES ARE ON THE SAME PLANE
% If all the particles are lying on the same physical plane in your
% calibration, we can assess the model performance by running it on the
% calibration stack itself, and compare results to true values because each
% particles at a given frame has the same Z coordinate

model.processing.cm_guess = .5;  
model.processing.cm_final = .9; 

%you can change the last array given as argument to process on more or less
%images of the flow (here the flow IS the calibration stack)
dat_valid = dtracker_process(model,img_train,1:N_cal);

figure
plot((dat_valid.fr-1)/(N_cal-1),dat_valid.Z,'.',[0 1],[0 1])
grid on
xlabel('Z_{true}')
ylabel('Z')



%% Load the experimental images

myfolder = './/flow//';
img_exp = dtracker_create('imageset',myfolder);

%% Check the processing parameters on a subset of data
%  Set the processing parameters and run the evluation on a subset of
%  frames
model_wte2.processing.cm_guess = .5;  
model_wte2.processing.cm_final = .9; 
dat_check = dtracker_process(model, img_exp, 1:4);
%the third argument is a vector that enables you to run dtracker_process on
%the corresponding frames of the flow. 

%  Inspect the results and change setting if not satisfied.
dtracker_show(dat_exp, img_exp)
%% Create tracking to track
%  Create a default tracking and edit the relevant fields
tracking = dtracker_create('tracking');
tracking.tracking_step = 1;
tracking.bounding_box = [-30 30 -30 30 -0.12 0.12]; %  [DXmin DXmax DYmin DYmax DZmin DZmax]
dat_exp_track = dtracker_postprocess(tracking,dat_exp);

dtracker_show(dat_exp_track, img_exp)
%% create scaling to obtain data in real units

%enter experience informations
camera_pixel_size = 6.5; %pixel size of the camera
magnification = 10; %magnification of the microscope

%get first image of the flow to collect information on images
first_im = fullfile(img_exp.path, img_exp.images{1});
info = imfinfo(first_im);
w = info.Width;
h = info.Height;

%  Create a default scaling and edit the relevant fields
scaling = dtracker_create('scaling');
scaling.X_to_x = w*camera_pixel_size/magnification; 
scaling.Y_to_y = h*camera_pixel_size/magnification;
scaling.Z_to_z = 40; %height change between first and last image of calibration stack in um
scaling.dt = 1/60; %time between 2 images
scaling.unit = 'um'; %select unit

%% Run processing on the whole flow
%  Add the tracking and scaling to the model
model.tracking = tracking;
model.scaling = scaling;

%  Run the processing on all data frames
dat_exp = dtracker_process(model,img_exp);

%  Plot the tracks
figure
dtracker_show(dat_exp,'plot_3d_tracks')

%% Compute errors
%this section is used if you want to compute error of defocustracker on a
%simulation. to do so, select the true values data that microsig generates
%when running a simulation with it
[filename, pathname] = uigetfile('*.txt','Select true values data (*.txt)');
if filename==0, return, end
name_dat = [pathname,filename];
dat_deftrack = dat_check;
%!! be carefull, the errors should be computed with unscaled data. we
%recommend to use dat_check of section "Check the processing parameters on a
%subset of data" and making the process run on all frames. Verify that no
%scaling structure exists when doing that.
dat_true = readtable(name_dat);
[errors, errors_delta] = dtracker_postprocess('compare_true_values',...
    dat_deftrack, dat_true);




