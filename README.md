# EPFL Bachelor Project 2022 - Adapted Defocustracker and Microsig for MICROBS Lab
This repository contains all the code that was adapted by Nils Delage and Saria Sfeir for the MICROBS Lab as part of their Bachelor Project at EPFL.  

The Original programs made by Massimilano Rossi and Rune Barnkob are [Defocustracker](https://gitlab.com/defocustracking/defocustracker-matlab) and [Microsig](https://gitlab.com/defocustracking/microsig-matlab).
